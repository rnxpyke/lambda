with import <nixpkgs> {};
stdenv.mkDerivation rec {
	name = "haskell-env";
	buildInputs = with pkgs; [
		haskellPackages.ghc
		haskellPackages.stack
		#haskellPackages.cabal
	];
}


