# Lambda

This is a haskell implementation of the [basic lambda calculus](https://en.wikipedia.org/wiki/Lambda_calculus).
The programm interprets standard input as a list of lambda terms, reducing each term until no further reductions are possible. 
It may be possible that you create an endless loop, causing the interpreter to never halt.

For the syntax of the input language, see the `test.lc` file or look in the sourcecode directly.
If you want to write a lambda term across multiple lines, enclose it with parentheses first.

## Building

Use Cabal or Stack to build the project.
- [Stack](https://docs.haskellstack.org/en/stable/README/) builds are reproducible, but pull in all dependencies in a local cache. 
    ```bash
    stack build
    stack exec lambda-exe
    ```
- [Cabal](https://www.haskell.org/cabal/) uses the system cache, but might need something like `ghc-static` on or the `-dynamic` flag to run properly. The `.cabal` file might have to be generated with `hpack` from the `package.yaml` first.
    ```bash
    cabal build
    cabal run
    ```
