module Main where

import Lib
import Parser
import Control.Monad
import Text.Parsec

echo = getLine >>= putStrLn

main :: IO ()
main = VarString <$$$$> (parse line "" <$>) <$> lines <$> getContents
     >>= foldM_ stepMayEval [] 
        where (<$$$$>) = fmap . fmap . fmap . fmap

oldmain :: IO ()
oldmain =  lines <$> getContents
        >>= mapM_ (evalPrint . (VarString <$$>) . parseExpr)
    where (<$$>) = (<$>) . (<$>)

evalPrint :: Maybe (Expr VarString) -> IO ()
evalPrint (Just expr) = print $ eval [] expr
evalPrint a = print a

parseEvalPrint :: Maybe (Expr VarString) -> IO ()
parseEvalPrint (Just expr) = do 
                print $ classify ([],[]) expr
                print $ eval [] expr
                print $ classify ([],[])  expr
parseEvalPrint a = print a

stepEval :: (Eq a, Renameable a, Show a) => Env a -> ParseLine a -> IO (Env a)
stepEval env (Import s)  = return env 
stepEval env (Let v exp) = return $ (v,exp) : env
stepEval env (Term exp)  = print (eval env exp) *> return env

stepMayEval :: (Eq a, Renameable a, Show a) => 
                Env a -> Either ParseError (ParseLine a) -> IO (Env a)
stepMayEval env (Right a) = stepEval env a
stepMayEval env x = print x *> return env
