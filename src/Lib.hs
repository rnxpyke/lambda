{-# LANGUAGE DeriveFunctor,FlexibleInstances #-}

module Lib
    ( ParseLine (..)
    , Expr (..)
    , rename
    , Renameable (..)
    , Bind (..)
    , Env (..)
    , VarString (..)
    , eval
    , classify
    ) where

import Data.List (union, dropWhileEnd)
import Data.Char (isDigit)

someFunc :: IO ()
someFunc = putStrLn "someFunc"

data ParseLine a = Import String | Let a (Expr a) | Term (Expr a) deriving (Eq, Show, Functor)

--a lambda expression
data Expr a = Var a | Lam a (Expr a) | App (Expr a) (Expr a) deriving (Eq, Functor)

--used to not show quotes on strings
newtype VarString = VarString String deriving Eq

instance Show VarString where
    show (VarString s) = s 

instance Show a => Show (Expr a) where 
    show (Var s) = show s
    show (Lam var x) = "\\" ++ show var  ++ showB x 
        where showB (Lam var x) = " " ++ show var ++ showB x
              showB x = "." ++ show x
    show (App x y) = showL x ++  showR y
        where showL (Lam _ _) = "(" ++ show x ++ ")"
              showL _ = show x
              showR (Var s) = ' ': show s
              showR _ = "(" ++ show y ++ ")"  


type Bind a = (a, Expr a)
type Env a = [Bind a]

eval :: (Eq a, Renameable a) => Env a -> Expr a -> Expr a
eval env (App x y) | Lam var f <- eval env x = eval env $ beta env (var, y) f 
                   | otherwise = App (eval env x) (eval env y)
eval env (Var var) | Just x <- lookup var env = eval env x
eval env (Lam v x) = Lam v (eval env x)
eval _ x = x

--todo: rename correctly
beta :: (Eq a, Renameable a) => Env a -> Bind a -> Expr a -> Expr a
beta env (var, exp) (Var v) | v == var = exp
                            | otherwise = (Var v)
beta env (var, exp) (Lam v e) | v == var = Lam v e
                              | elem v $ finl = 
                                    Lam vnew $ 
                                    beta env (var,exp) $ rename v vnew e
                              | otherwise = Lam v (beta env (var, exp) e)
                                where   finl = snd $ classify ([],[]) exp
                                        finr = snd $ classify ([],[]) e
                                        vnew = newName v $ union finl finr  
beta env (var, exp) (App m n) = App (beta env (var,exp) m) (beta env (var,exp) n)

--rename function
class Renameable a where
    newName :: a -> [a] -> a 
instance Renameable String where 
    newName x ys = head $ filter ( `notElem` ys) $ (s ++) . show <$> [1..] where
        s = dropWhileEnd isDigit x
instance Renameable VarString where
    newName x ys = VarString $ newName (unwrap x) (unwrap <$> ys) 
        where unwrap (VarString x) = x


--todo: test if this works correctly
rename :: (Eq a) => a -> a -> Expr a -> Expr a
rename o n (App x y) = App (rename o n x) (rename o n y)
rename o n (Lam var x) =  if var == o then Lam var x 
                                      else Lam var (rename o n x)
rename o n (Var x) = if x == o then (Var n) 
                              else (Var x)


classify :: (Eq a) => ([a],[a]) -> Expr a -> ([a],[a])
classify (bound, unbound) (Var x) | elem x bound = (bound, unbound)
                                  | elem x unbound = (bound, unbound)
                                  | otherwise = (bound, x : unbound)
classify (bound, unbound) (Lam var e) | elem var bound = classify (bound,unbound) e
                                      | otherwise = classify (var : bound, unbound) e
classify (bound, unbound) (App x y) = (union lbound rbound, union lunbound runbound)
    where (lbound, lunbound) = classify (bound, unbound) x
          (rbound, runbound) = classify (bound, unbound) y
