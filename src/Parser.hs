module Parser 
    ( parseExpr
    , term
    , require
    , line
    ) where

import Lib
import Text.Parsec
--import Text.Parsec.Text
import Text.Parsec.String
import Data.List

lamp :: Parser String
lamp = string "\\" <* white

term :: Parser (Expr String)
term = appfold 
   <|> do {lamp
        ; x <- lcid
        ; char '.' <* white
        ;y <- term
        ; return $ Lam x y}

lcid :: Parser String
lcid = many1 alphaNum <* white

appfold :: Parser (Expr String)
appfold = foldl1 App <$> aterm `sepBy1` white 

--appterm :: Parser Expr
--appterm = try (do 
--            {x <- aterm
--            ;white
--            ;y <- appterm
--            ;return $App x y})
--        <|> aterm 

aterm :: Parser (Expr String)
aterm = try $ parens term 
    <|> Var <$> (addp (option "" $ string "_") lcid)

addp :: Parser [a] -> Parser [a] -> Parser [a]
addp x y = do a <- x
              b <- y
              return $ a ++ b

parens :: Parser a -> Parser a
parens a = do {char '(' <* white
              ;x <- a
              ;char ')' <* white
              ;return x}

white :: Parser String
white = many $ oneOf " \n\t"

toMaybe :: Either b a -> Maybe a
toMaybe (Right x) = Just x
toMaybe _ = Nothing

parseExpr :: String -> Maybe (Expr String)
parseExpr = toMaybe . (parse term "") 

--

lineEnd :: Parser String
lineEnd = const "" <$> eof <|> string "\\n"

require :: Parser (ParseLine a)
require = string ":i " *> (Import <$> manyTill anyChar (try lineEnd))

binding :: Parser (ParseLine String)
binding =  do 
              var <- lcid
              _ <- char '=' <* white
              term <- term
              return (Let var term)

line :: Parser (ParseLine String)
line = try require <|> try binding <|> (Term <$> term)
